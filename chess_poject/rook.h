#pragma once
#include "piece.h"

#ifndef ROOK_H
#define ROOK_H


class Rook : public Piece {
public:
	Rook(bool color, char type, Point& pos);
	virtual ~Rook();
	virtual bool can_move(Piece& dst);
};

#endif // !ROOK_H