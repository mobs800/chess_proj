#include "Pawn.h"
#include <iostream>

Pawn::Pawn(bool color, char type, Point& pos) : Piece(color, type, pos) {
	_moved = false;
}


Pawn::~Pawn() {

}

bool Pawn::can_move(Piece& dst) {
	int sx = _pos.get_x();
	int sy = _pos.get_y();
	int dx = dst.get_pos().get_x();
	int dy = dst.get_pos().get_y();

	std::cout << 's' << sx << std::endl;
	std::cout << 's' << sy << std::endl;
	std::cout << 'd' << dx << std::endl;
	std::cout << 'd' << dy << std::endl;

	if (!_moved && dx == sx && (dy - 1 == sy || dy - 2 == sy) && _isBlack && dst.isEmpty()) {
		_moved = true;

		return true;
	}

	else if (_moved && dx == sx && dy - 1 == sy && _isBlack && dst.isEmpty()) {
		_moved = true;

		return true;
	}

	else if ((dx + 1 == sx || dx - 1 == sx) && dy - 1 == sy && !dst.isEmpty() && dst.get_color() != _isBlack && _isBlack && dx != sx) {
		_moved = true;

		return true;
	}

	else if (!_moved && dx == sx && (dy + 1 == sy || dy + 2 == sy) && !_isBlack && dst.isEmpty()) {
		_moved = true;

		return true;
	}

	else if (_moved && dx == sx && dy + 1 == sy && !_isBlack && dst.isEmpty()) {
		_moved = true;

		return true;
	}

	else if ((dx + 1 == sx || dx - 1 == sx) && dy + 1 == sy && !dst.isEmpty() && dst.get_color() != _isBlack && !_isBlack && dx != sx ) {
		_moved = true;

		return true;
	}

	else
		return false;
}