#include "Board.h"
#include <iostream>
#include <vector>

Board::Board() {
	_isBlackTurn = false;
	this->initPieces();
}

Board::~Board() {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			delete _board[i][j];
		}	
	}  
}

void Board::initPieces() {

	Point p(0, 0);

	for (int i = 0; i < 8; i++)
	{
		_board[1][i] = new Pawn(true, 'p', p = Point(i, 1));
		_board[6][i] = new Pawn(false, 'p', p = Point(i, 6));
	}

	for (int i = 2; i < 6; i++) {
		for (int j = 0; j < 8; j++) {
			_board[i][j] = new EmptyPiece(p = Point(j, i));
		}
	}

	_board[0][0] = new Rook(true, 'r', (p = Point(0,0)));
	_board[0][1] = new Knight(true, 'n', (p = Point(1, 0)));
	_board[0][2] = new Bishop(true, 'b', (p = Point(2, 0)));
	_board[0][4] = new Queen(true, 'q', (p = Point(4, 0)));
	_board[0][3] = new King(true, 'k', (p = Point(3, 0)));
	_board[0][5] = new Bishop(true, 'b', (p = Point(5, 0)));
	_board[0][6] = new Knight(true, 'n', (p = Point(6, 0)));
	_board[0][7] = new Rook(true, 'r', (p = Point(7, 0)));
	
	_board[7][0] = new Rook(false, 'R', (p = Point(0, 7)));
	_board[7][1] = new Knight(false, 'N', (p = Point(1, 7)));
	_board[7][2] = new Bishop(false, 'B', (p = Point(2, 7)));
	_board[7][4] = new Queen(false, 'Q', (p = Point(4, 7)));
	_board[7][3] = new King(false, 'K', (p = Point(3, 7)));
	_board[7][5] = new Bishop(false, 'B', (p = Point(5, 7)));
	_board[7][6] = new Knight(false, 'N',(p = Point(6, 7)));
	_board[7][7] = new Rook(false, 'R', (p = Point(7, 7)));

	this->blackKing = _board[0][3];
	this->whiteKing = _board[7][3];


}

bool Board::blocked(Piece& src, Piece& dest) {


  int destY = dest.get_pos().get_y();
  int destX = dest.get_pos().get_x();
  int srcY = src.get_pos().get_y();
  int srcX = src.get_pos().get_x();
  int dy, dx, n;


  if (destY - srcY == 0)
    dy = 0;
  else
    dy = (destY - srcY) / std::abs(destY - srcY);
  
  if (destX - srcX == 0)
    dx = 0;
  else
    dx = (destX - srcX) / std::abs(destX - srcX);
  
  if (dy == 0)
    n = std::abs(destX - srcX);
  else
    n = std::abs(destY - srcY);

  int currX = srcX + dx;
  int currY = srcY + dy;

  for (int i = 0; i < n - 1; i++)
  {
    if (!_board[currY][currX]->isEmpty())
      return true;
    currX += dx;
    currY += dy;
  }

  if (_board[currY][currX]->isEmpty())
    return false;
  if (_board[srcY][srcX]->get_color() != _board[currY][currX]->get_color())
    return false;

  return true;
}

void Board::printBoard() {	
	std::cout << "  A B C D E F G H" << std::endl;
	for (int i = 0; i < 8; i++) {

		std::cout << i + 1 << " ";
		
		for (int j = 0; j < 8; j++) {
		
			if (!(_board[i][j]->get_color()))
				std::cout << (char)toupper(_board[i][j]->get_type()) << " ";
			else
				std::cout << (_board[i][j]->get_type()) << " ";

		}
		std::cout << std::endl;
	}
}

std::vector<Point> Board::convertor(std::string name) {
	int Xsrc = (int)(name[0] - 97);
	int Ysrc = (int)(name[1] - 49);
	int Xdst = (int)(name[2] - 97);
	int Ydst = (int)(name[3] - 49);

	Point p(0, 0);

	std::vector<Point> points;

	points.emplace_back(Point(Xsrc, Ysrc));
	points.emplace_back(Point(Xdst, Ydst));
	
	
	return points;
}

bool Board::checkCheck(Piece& king) {
	Piece* p = nullptr;

	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			p = _board[i][j];
			if (!p->isEmpty() && p->get_color() != king.get_color()) {
				if (p->can_move(king) && !blocked(*p, king) && p->get_type() != 'n' && p->get_type() != 'N')
					return true;
				else if(p->can_move(king) && (p->get_type() == 'n' || p->get_type() == 'N'))
					return true;
			}
		}
	}
	return false;

}

int Board::do_move(std::string move) {
	std::vector<Point> points;
	points = convertor(move);

	int srcX = points[0].get_x();
	int srcY = points[0].get_y();
	int destX = points[1].get_x();
	int destY = points[1].get_y();

	Piece* dst = _board[destY][destX];
	Piece* src = _board[srcY][srcX];

	Piece* myKing = nullptr;
	Piece* enKing = nullptr;

	if (whiteKing->get_color() == _isBlackTurn) {
		myKing = whiteKing;
		enKing = blackKing;
	}
	else {
		myKing = blackKing;
		enKing = whiteKing;
	}

	if (srcX >= 8 || srcX < 0 || srcY >= 8 || srcY < 0 || destX >= 8 || destX < 0 || destY >= 8 || destY < 0)
		return 5;

	else if (_board[srcY][srcX]->get_color() != _isBlackTurn)
		return 2;

	else if (!_board[destY][destX]->isEmpty() && _board[destY][destX]->get_color() == _isBlackTurn)
		return 3;

	else if (srcX == destX && srcY == destY)
		return 7;

	else if (src->get_type() != 'n' && src->get_type() != 'N') {
		if (!(src->can_move(*dst)) || this->blocked(*src, *dst))
			return 6;
	}

	else if (!(src->can_move(*dst)))
		return 6;

	std::cout << "x: " << dst->get_pos().get_x() << std::endl;
	std::cout << "y: " << dst->get_pos().get_y() << std::endl;

	std::cout << "x1: " << dst->get_pos().get_x() << std::endl;
	std::cout << "y1: " << dst->get_pos().get_y() << std::endl;

	bool val = this->testMove(*src, *dst, *myKing);
	if (val)
		return 4;

	if (_isBlackTurn)
		_isBlackTurn = false;
	else
		_isBlackTurn = true;

	if (this->checkCheck(*enKing))
		return 1;
	else
		return 0;
	
	

}


bool Board::testMove(Piece& src, Piece& dest, Piece& king)
{	 
	Point srcPos = src.get_pos();
	int srcX = src.get_pos().get_x();
	int srcY = src.get_pos().get_y();
	int destX = dest.get_pos().get_x();
	int destY = dest.get_pos().get_y();

	src.get_pos().setNewCord(destX, destY);
	Piece* p = new EmptyPiece(srcPos);

	std::cout << "x2: " << destX << std::endl;
	std::cout << "y2: " << destY << std::endl;

	this->_board[destY][destX] = &src;
	_board[srcY][srcX] = p;

	bool ret = checkCheck(king); 

	if (ret)
	{
			delete _board[srcY][srcX];
			src.get_pos().setNewCord(srcX, srcY);
			_board[srcY][srcX] = &src;
			_board[destY][destX] = &dest;
	}
	else
	{
		Piece* temp = &dest;
		delete temp;
	}
	return ret;
}
