#pragma once
#include "piece.h"
class Bishop : public Piece	{
public:
	Bishop(bool color, char type, Point& pos);
	virtual ~Bishop();
	virtual bool can_move(Piece& dst);
};

