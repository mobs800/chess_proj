#pragma once
#ifndef PIECE_H
#define PIECE_H

#include "point.h"

class Piece {
protected:
	bool _isBlack;
	char _type;
	Point _pos;


public:
	Piece(bool color, char type, Point& pos);
	virtual ~Piece();
	bool get_color();
	char get_type();
	Point& get_pos();
	virtual bool can_move(Piece& dst) = 0;
	virtual bool isEmpty();
};


#endif // !PIECE_H
