#include "piece.h"

Piece::Piece(bool color, char type, Point& pos) : _pos(pos) {
	_isBlack = color;
	_type = type;
}

Piece::~Piece() {

}

bool Piece::get_color() {
	return _isBlack;
}

char Piece::get_type() {
	return _type;
}

Point& Piece::get_pos() {
	return _pos;
}

bool Piece::isEmpty()
{
	return false;
}