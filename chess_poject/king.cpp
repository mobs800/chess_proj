#include "king.h"

#include <iostream>
#include <cmath>

King::King(bool color, char type, Point& pos) : Piece(color, type, pos) 
{

}

King::~King()
{

}

bool King::can_move(Piece& dst)
{
    int dx = std::abs(dst.get_pos().get_x() - _pos.get_x());
    int dy = std::abs(dst.get_pos().get_y() - _pos.get_y());

    if (dx <= 1 && dy <= 1)
        return true;
    return false;
}