#pragma once
#include "piece.h"
class Pawn : public Piece {
private:
	bool _moved;
public:
	Pawn(bool color, char type, Point& pos);
	~Pawn();
	virtual bool can_move(Piece& dst);
};

