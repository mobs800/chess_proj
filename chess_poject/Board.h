#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Piece.h"
#include "rook.h"
#include "Queen.h"
#include "Pawn.h"
#include "Knight.h"
#include "King.h"
#include "EmptyPiece.h"
#include "Bishop.h"
#include "Point.h"


class Board	{
private:
		Piece* _board[8][8];
		bool _isBlackTurn;
		Piece* whiteKing;
		Piece* blackKing;

	public:
		Board();
		~Board();
		void initPieces();
		bool blocked(Piece& src, Piece& dest);
		void printBoard();
		int do_move(std::string move);
		std::vector<Point> convertor(std::string name);
		bool checkCheck(Piece& king);
		bool testMove(Piece& src, Piece& dest, Piece& king);
};

