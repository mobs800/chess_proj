#pragma once
#include "piece.h"
class King : public Piece
{
	public:
		King(bool color, char type, Point& pos);
		virtual ~King();
		virtual bool can_move(Piece& dst);
};

