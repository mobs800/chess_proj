#include "EmptyPiece.h"

EmptyPiece::EmptyPiece(Point& pos) : Piece(false, 'e', pos)
{

}

EmptyPiece::~EmptyPiece() {

}

bool EmptyPiece::can_move(Piece& dst)
{
	return false;
}

bool EmptyPiece::isEmpty()
{
	return true;
}

