#pragma once
class Point {
private: 
	int _x;
	int _y;
public:
	Point(int x, int y);
	Point(const Point& other);
	~Point();
	int get_x() const;
	int get_y() const;
	void setNewCord(int x, int y);
};

