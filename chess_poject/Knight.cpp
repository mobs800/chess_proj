#include "Knight.h"

#include <iostream>
#include <cmath>

Knight::Knight(bool color, char type, Point& pos) : Piece(color, type, pos) {
	
}

Knight::~Knight()
{

}

bool Knight::can_move(Piece& dest)
{	
	int dy = dest.get_pos().get_y();
	int dx = dest.get_pos().get_x();
	int sx = _pos.get_x();
	int sy = _pos.get_y();

	if ((dx - 1 == sx && dy - 2 == sy) || (dx - 2 == sx && dy - 1 == sy) ||
		(dx - 2 == sx && dy + 1 == sy) || (dx - 1 == sx && dy + 2 == sy) ||
		(dx + 1 == sx && dy + 2 == sy) || (dx + 2 == sx && dy + 1 == sy) ||
		(dx + 2 == sx && dy - 1 == sy) || (dx + 1 == sx && dy - 2 == sy))
	{
		if (dest.isEmpty() || this->_isBlack != dest.get_color())
			return true;
	}
	else
		return false;
}