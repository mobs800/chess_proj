#pragma once
#include "piece.h"
class Queen : public Piece {
public:
	Queen(bool color, char type, Point& pos);
	virtual ~Queen();
	virtual bool can_move(Piece& dst);
};

