#pragma once

#include "piece.h"

class Knight : public Piece
{
public:
	Knight(bool color, char type, Point& pos);
	virtual ~Knight();
	virtual bool can_move(Piece& dest);
};

