#include "Point.h"

Point::Point(int x, int y) {
	_x = x;
	_y = y;
}

Point::Point(const Point& other)
{
	_x = other.get_x();
	_y = other.get_y();
}


Point::~Point() {

}

int Point::get_x() const {
	return _x;
}

int Point::get_y() const {
	return _y;
}

void Point::setNewCord(int x, int y) {
	_x = x;
	_y = y;
}
