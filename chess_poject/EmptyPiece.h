#pragma once

#include "piece.h"

class EmptyPiece : public Piece
{
public:

	EmptyPiece(Point& pos);
	virtual ~EmptyPiece();
	virtual bool can_move(Piece& dst);
	virtual bool isEmpty();

};

