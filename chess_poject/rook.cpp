#include "rook.h"
#include <iostream>



Rook::Rook(bool color, char type, Point& pos) : Piece(color, type, pos) 
{

}

Rook::~Rook() 
{

}


bool Rook::can_move(Piece& dst) {
    int dx = std::abs(dst.get_pos().get_x() - _pos.get_x());
    int dy = std::abs(dst.get_pos().get_y() - _pos.get_y());

    if (dx * dy == 0)
        return true;
    return false;
}