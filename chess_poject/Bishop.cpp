#include "Bishop.h"
#include <iostream>

Bishop::Bishop(bool color, char type, Point& pos) : Piece(color, type, pos) {
	
}

Bishop::~Bishop() {

}

bool Bishop::can_move(Piece& dst) {
	int dx = std::abs(dst.get_pos().get_x() - _pos.get_x());
	int dy = std::abs(dst.get_pos().get_y() - _pos.get_y());

	if (dx * dy != 0 && dx == dy)
		return true;
	
	else
		return false;
}
