#include "Queen.h"
#include <iostream>

Queen::Queen(bool color, char type, Point& pos) : Piece (color, type, pos) {

}

Queen::~Queen() {

}

bool Queen::can_move(Piece& dst) {
	int dx = std::abs(dst.get_pos().get_x() - _pos.get_x());
	int dy = std::abs(dst.get_pos().get_y() - _pos.get_y());

	if (dx * dy != 0 && dx == dy)
		return true;

	else if (dx * dy == 0)
		return true;

	else
		return false;

}


